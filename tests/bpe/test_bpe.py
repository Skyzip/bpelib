import sys, os, shutil
from pathlib import Path
import tempfile
bpe_lib = str(Path(Path(__file__).absolute()).parent.parent.parent)
print("appending dir to path: " + bpe_lib)
sys.path.append(bpe_lib)
from bpe.bpe import BPE, BPEB, WordFreq, bigram


def test_WordFreq_attr():
    wf = WordFreq({})

    assert hasattr(wf, 'merge_pair')


def test_WordFreq_subclass():
    assert isinstance(WordFreq(), dict)


def test_WordFreq_merge_pair_bytes():
    wf = WordFreq({b'm e r g e _ g e': 1, b'l e a v e _ m e': 3})
    wf.merge_pair((b'g', b'e'))

    assert wf.get(b'm e r ge _ ge') == 1
    assert wf.get(b'l e a v e _ m e') == 3


def test_BPE_attr():
    bpe = BPE()

    assert hasattr(bpe, 'all_bytes')
    assert hasattr(bpe, '__call__')
    assert hasattr(bpe, '_make_pairs')
    assert hasattr(bpe, '_vocab_add')
    assert hasattr(bpe, 'init')
    assert hasattr(bpe, 'learn_encoding')
    assert hasattr(bpe, 'encode')
    assert hasattr(bpe, 'decode')
    assert hasattr(bpe, 'save')
    assert hasattr(bpe, 'load')
    assert hasattr(bpe, 'vocab')
    assert hasattr(bpe, 'merges')
    assert hasattr(bpe, 'sow')
    assert hasattr(bpe, 'eow')


def test_BPE_all_bytes():
    all_bytes = BPE.all_bytes()

    assert all_bytes == {bytes([i]): i for i in range(256)}
    assert isinstance(all_bytes, dict)


def test_BPE__init__():
    bpe = BPE()

    assert bpe._max_vocab_size == 4096
    assert bpe._sow == b'<w/>'
    assert bpe._eow == b'</w>'
    assert bpe._enc == 'utf8'
    assert bpe._word_freq is None

    all_bytes = BPE.all_bytes()
    size = len(all_bytes)
    default_dict = all_bytes.copy()
    default_dict.update({bpe._sow: size, bpe._eow: size + 1})

    assert isinstance(bpe._vocab, dict)
    assert bpe._vocab == default_dict
    assert bpe._vocab_size == size + 2
    assert not bpe._merges


def test_BPE_make_pairs():
    wf = WordFreq({(b'a b p'): 3, (b'c d q'): 1, (b'c d'): 4, (b'a b'): 2})
    pairs = BPE._make_pairs(wf)

    assert pairs == {(b'a', b'b'): 5, (b'c', b'd'): 5, (b'b', b'p'): 3, (b'd', b'q'): 1}


def test_BPE_learn_encoding():
    bpe = BPE()
    initial_vocab = bpe.vocab
    bpe.learn_encoding(['most', 'frequent', 'is', 'st', 'st', 'second', 'is', 'iS'], 260)

    default_dict = BPE.all_bytes().copy()
    size = len(default_dict)

    assert bpe._merges == {(b't', bpe._eow): 0, (bpe._sow, b'i'): 1}
    initial_vocab.update({b't' + bpe._eow: size + 2, bpe._sow + b'i': size + 3})
    assert bpe._vocab == initial_vocab

    bpe_copy = BPE(['most', 'frequent', 'is', 'st', 'st', 'second', 'is', 'iS'], 260)
    assert bpe_copy == bpe


def test_BPE_learn_encoding_no_pairs():
    bpe = BPE(['p'])
    bpe_cmp = BPE()
    size = len(bpe_cmp._vocab)
    bpe_cmp._vocab.update({bpe._sow + b'p': size, bpe._sow + b'p' + bpe._eow: size + 1})
    bpe_cmp._merges.update({(bpe._sow, b'p'): 0, (bpe._sow + b'p', bpe._eow): 1})
    bpe_cmp._vocab_size = len(bpe_cmp._vocab)

    assert bpe == bpe_cmp


def test_BPE_encode():
    bpe = BPE(['p'])

    encoded = bpe('p will be encoded')
    assert encoded == (
            bpe.sow + 'p' + bpe.eow + ' ' +
            bpe.sow + ' w i l l ' + bpe.eow + ' ' +
            bpe.sow + ' b e ' + bpe.eow + ' ' +
            bpe.sow + ' e n c o d e d ' + bpe.eow
    )

    bpe = BPE(['p'], encoding='ascii')

    encoded = bpe('p will be encoded', True, encoding='ascii')
    assert encoded == (
            bpe.sow + 'p' + bpe.eow + ' ' +
            bpe.sow + ' w i l l ' + bpe.eow + ' ' +
            bpe.sow + ' b e ' + bpe.eow + ' ' +
            bpe.sow + ' e n c o d e d ' + bpe.eow
    )


def test_BPE_enc_n_dec():
    bpe = BPE(['this', 'is', 'the', 'bpe'])

    to_encode = 'please try encoding this sentence'
    encoded = bpe(to_encode)
    decoded = bpe(encoded)

    assert to_encode == decoded

    bpe = BPE(['this', 'is', 'the', 'bpe'], encoding='ascii')

    to_encode = 'please try encoding this sentence'
    encoded = bpe(to_encode, True, encoding='ascii')
    decoded = bpe(encoded, False, encoding='ascii')

    assert to_encode == decoded


def test_BPE__eq__():
    bpe1 = BPE()
    bpe2 = BPE()
    not_bpe = []

    assert bpe1 == bpe2
    assert bpe1 != not_bpe


def test_BPE_save_n_load():
    bpe = BPE(['this', 'is', 'the', 'bpe'])
    bpe_clone = BPE(['this', 'is', 'the', 'bpe'])

    temp_dir = tempfile.gettempdir()
    root_save_dir = temp_dir + os.path.sep + 'bpe_test'
    save_dir = root_save_dir + os.path.sep + 'save'
    bpe.save(save_dir)
    del bpe
    bpe = BPE()
    bpe.load(save_dir)

    shutil.rmtree(root_save_dir)

    assert bpe_clone._vocab == bpe._vocab
    assert bpe_clone._merges == bpe._merges
    assert bpe_clone._vocab_size == bpe._vocab_size


def test_BPEB_attr():
    bpe = BPEB()

    assert hasattr(bpe, 'all_bytes')
    assert hasattr(bpe, '__call__')
    assert hasattr(bpe, '_make_pairs')
    assert hasattr(bpe, '_vocab_add')
    assert hasattr(bpe, 'init')
    assert hasattr(bpe, 'learn_encoding')
    assert hasattr(bpe, 'encode')
    assert hasattr(bpe, 'decode')
    assert hasattr(bpe, 'save')
    assert hasattr(bpe, 'load')
    assert hasattr(bpe, 'vocab')
    assert hasattr(bpe, 'merges')
    assert hasattr(bpe, 'sow')
    assert hasattr(bpe, 'eow')


def test_BPEB_all_bytes():
    all_bytes = BPEB.all_bytes()

    assert all_bytes == {bytes([i]): i for i in range(256)}
    assert isinstance(all_bytes, dict)


def test_BPEB__init__():
    bpe = BPEB()

    assert bpe._max_vocab_size == 4096
    assert bpe._sow == b'<w/>'
    assert bpe._eow == b'</w>'
    assert not hasattr(bpe, '_enc')
    assert bpe._word_freq is None

    all_bytes = BPEB.all_bytes()
    size = len(all_bytes)
    default_dict = all_bytes.copy()
    default_dict.update({bpe._sow: size, bpe._eow: size + 1})

    assert isinstance(bpe._vocab, dict)
    assert bpe._vocab == default_dict
    assert bpe._vocab_size == size + 2
    assert not bpe._merges


def test_BPEB_make_pairs():
    wf = WordFreq({(b'a b p'): 3, (b'c d q'): 1, (b'c d'): 4, (b'a b'): 2})
    pairs = BPEB._make_pairs(wf)

    assert pairs == {(b'a', b'b'): 5, (b'c', b'd'): 5, (b'b', b'p'): 3, (b'd', b'q'): 1}


def test_BPEB_learn_encoding():
    bpe = BPEB()
    initial_vocab = bpe.vocab
    bpe.learn_encoding([b'most', b'frequent', b'is', b'st', b'st', b'second', b'is', b'iS'], 260)

    default_dict = BPEB.all_bytes().copy()
    size = len(default_dict)

    assert bpe._merges == {(b't', bpe._eow): 0, (bpe._sow, b'i'): 1}
    initial_vocab.update({b't' + bpe._eow: size + 2, bpe._sow + b'i': size + 3})
    assert bpe._vocab == initial_vocab

    bpe_copy = BPEB([b'most', b'frequent', b'is', b'st', b'st', b'second', b'is', b'iS'], 260)
    assert bpe_copy == bpe


def test_BPEB_learn_encoding_no_pairs():
    bpe = BPEB([b'p'])
    bpe_cmp = BPEB()
    size = len(bpe_cmp._vocab)
    bpe_cmp._vocab.update({bpe._sow + b'p': size, bpe._sow + b'p' + bpe._eow: size + 1})
    bpe_cmp._merges.update({(bpe._sow, b'p'): 0, (bpe._sow + b'p', bpe._eow): 1})
    bpe_cmp._vocab_size = len(bpe_cmp._vocab)

    assert bpe == bpe_cmp


def test_BPEB_encode():
    bpe = BPEB([b'p'])

    encoded = bpe(b'p will be encoded')
    assert encoded == (
            bpe.sow + b'p' + bpe.eow + b' ' +
            bpe.sow + b' w i l l ' + bpe.eow + b' ' +
            bpe.sow + b' b e ' + bpe.eow + b' ' +
            bpe.sow + b' e n c o d e d ' + bpe.eow
    )

    bpe = BPEB([b'p'])

    encoded = bpe(b'p will be encoded', True)
    assert encoded == (
            bpe.sow + b'p' + bpe.eow + b' ' +
            bpe.sow + b' w i l l ' + bpe.eow + b' ' +
            bpe.sow + b' b e ' + bpe.eow + b' ' +
            bpe.sow + b' e n c o d e d ' + bpe.eow
    )


def test_BPEB_enc_n_dec():
    bpe = BPEB([b'this', b'is', b'the', b'bpe'])

    to_encode = b'please try encoding this sentence'
    encoded = bpe(to_encode)
    decoded = bpe(encoded)

    assert to_encode == decoded

    bpe = BPEB([b'this', b'is', b'the', b'bpe'])

    to_encode = b'please try encoding this sentence'
    encoded = bpe(to_encode, True)
    decoded = bpe(encoded, False)

    assert to_encode == decoded


def test_BPEB__eq__():
    bpe1 = BPEB()
    bpe2 = BPEB()
    not_bpe = []

    assert bpe1 == bpe2
    assert bpe1 != not_bpe


def test_BPEB_save_n_load():
    bpe = BPEB([b'this', b'is', b'the', b'bpe'])
    bpe_clone = BPEB([b'this', b'is', b'the', b'bpe'])

    temp_dir = tempfile.gettempdir()
    root_save_dir = temp_dir + os.path.sep + 'bpe_test'
    save_dir = root_save_dir + os.path.sep + 'save'
    bpe.save(save_dir)
    del bpe
    bpe = BPEB()
    bpe.load(save_dir)

    shutil.rmtree(root_save_dir)

    assert bpe_clone._vocab == bpe._vocab
    assert bpe_clone._merges == bpe._merges
    assert bpe_clone._vocab_size == bpe._vocab_size


def test_bigram():
    corpus = ['word', 'collection']
    bigrams = bigram(corpus)

    assert bigrams == {('w', 'o'): 1, ('o', 'r'): 1, ('r', 'd'): 1,

                       ('c', 'o'): 1, ('o', 'l'): 1, ('l', 'l'): 1,
                       ('l', 'e'): 1, ('e', 'c'): 1, ('c', 't'): 1,
                       ('t', 'i'): 1, ('i', 'o'): 1,('o', 'n'): 1}
