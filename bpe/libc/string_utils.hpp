#ifndef STRING_UTILS_HPP
#define STRING_UTILS_HPP

char** split_word(const char* word, size_t* size);

#endif // !STRING_UTILS_HPP
